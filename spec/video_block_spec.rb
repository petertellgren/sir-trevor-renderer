require 'spec_helper'

describe SirTrevorRenderer::VideoBlock do

  it 'renders embed code for youtube' do
    @youtube_content = {"source" => "youtube", "remote_id" => "okJRhpGFN_c"}

    video_block = SirTrevorRenderer::VideoBlock.new

    expect(video_block.render(@youtube_content, {video_width: 700, video_height: 424})).to eq( youtube_output )
  end

  it 'renders embed code for vimeo' do
    @youtube_content = {"source" => "vimeo", "remote_id" => "97438456"}

    video_block = SirTrevorRenderer::VideoBlock.new

    expect(video_block.render(@youtube_content, {video_width: 500, video_height: 281})).to eq( vimeo_output )
  end

  def youtube_output
    "<iframe width='700' height='424' src='//www.youtube.com/embed/okJRhpGFN_c' frameborder='0' allowfullscreen></iframe>"
  end

  def vimeo_output
    "<iframe src='//player.vimeo.com/video/97438456?title=1&amp;byline=1&amp;portrait=1&amp;autoplay=0' width='500' height='281' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>"
  end

end
