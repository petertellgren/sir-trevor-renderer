require 'spec_helper'
require 'nokogiri'

describe SirTrevorRenderer do

  class MockBlock
    def initialize(template = nil)
    end
    def render()
    end
  end

  it 'has a version' do
    SirTrevorRenderer::VERSION.should_not be_nil
  end

  describe 'register' do
    it 'registers a renderer implementation by block type' do
      SirTrevorRenderer.register(MockBlock, 'mock')
    end

    it 'registers a renderer implementation by block type as symbol' do
      SirTrevorRenderer.register(MockBlock, :mock)
    end
  end

  describe 'registered?' do
    it 'checks precense of a regiesterd renderer' do
      SirTrevorRenderer.register(MockBlock, 'mock')
      expect(SirTrevorRenderer.registered?('mock')).to eq( true )
    end
  end

  describe '[]' do
    it "returns renderer object with correct block type" do
      SirTrevorRenderer.register(MockBlock, 'mock')
      impl = SirTrevorRenderer['mock']
      expect(MockBlock).to eq(impl.class)
    end
  end

  describe '#render' do
    it 'renders a html file from Sir Trevor JSON structure' do
      html = SirTrevorRenderer.render(test_json, {video_width: 700, video_height: 424})
      expect(normalize(html)).to eq( normalize(test_output) )
    end
  end


  def test_json
    JSON.generate({data:[
      {type: "heading", data:{ text: "Heading Block" } },
      {type: "text", data:{ text: "1st paragraph with **bold**, _italics_ and _**both**_. We also add some #tags, @mentions and [links](http://google.com).\n\n2nd paragraph inside the block\n\n\- item 1\n- item 2\n\n3rd paragraph\n"}},
      {type: "list", data:{ text: " - [item 1](#local)\n - _item 2_\n - **item 3**\n"}},
      {type: "video", data:{ source:"youtube", remote_id:"okJRhpGFN_c"}}
    ]})
  end

  def test_output
    <<-HTML
<h2>Heading Block</h2>
<p>1st paragraph with <strong>bold</strong>, <em>italics</em> and <em><strong>both</strong></em>. We also add some #tags, @mentions and <a href="http://google.com">links</a>.</p>

<p>2nd paragraph inside the block</p>

<ul>
<li>item 1</li>
<li>item 2</li>
</ul>

<p>3rd paragraph</p>
<ul>
<li><a href="#local">item 1</a></li>
<li><em>item 2</em></li>
<li><strong>item 3</strong></li>
</ul>
<iframe width='700' height='424' src='//www.youtube.com/embed/okJRhpGFN_c' frameborder='0' allowfullscreen></iframe>
    HTML
  end


  def normalize(html)
    Nokogiri::HTML.fragment(html).to_s.strip
  end

end