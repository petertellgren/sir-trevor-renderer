require 'spec_helper'

describe SirTrevorRenderer::ListBlock do

  before(:each) do
    @block_content = {"text" => " - [A](http://google.com)\n - _List_\n - **Block** #tag\n"}
  end

  it 'comes with a default renderer' do
    list_block = SirTrevorRenderer::ListBlock.new()
    expect(list_block.render(@block_content)).to eq( default_output )
  end

  def default_output
    "<ul>\n<li><a href=\"http://google.com\">A</a></li>\n<li><em>List</em></li>\n<li><strong>Block</strong> #tag</li>\n</ul>\n"
  end

end
