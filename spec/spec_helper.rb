require 'pry'
require 'json'
require 'sir_trevor_renderer'
require 'nokogiri'

def normalize(html)
  Nokogiri::HTML.fragment(html).to_s.strip
end