require 'spec_helper'

describe SirTrevorRenderer::HeadingBlock do

  before(:each) do
    @block_content = {"text" => "Heading with a #tag"}
  end

  it 'comes with a default renderer' do
    heading_block = SirTrevorRenderer::HeadingBlock.new()
    html = heading_block.render(@block_content)
    expect(normalize(html)).to eq(default_output)
  end

  def default_output
    html = <<-HTML
      <h2>Heading with a #tag</h2>
    HTML
    normalize(html);
  end

end
