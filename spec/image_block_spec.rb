require 'spec_helper'


describe SirTrevorRenderer::ImageBlock do

  before(:each) do
    @block_content = {'id' => 40, 'article_id' => 10, 'file' => {'url' => 'file_url', 'large' => { 'url' => 'large_file_url' }}}
  end

  it 'comes with a default renderer' do
    image_block = SirTrevorRenderer::ImageBlock.new()
    expect(image_block.render(@block_content)).to eq(default_template_output('file_url'))
  end

  it 'allows for custom file size option' do
    image_block = SirTrevorRenderer::ImageBlock.new()
    expect(image_block.render(@block_content, image_size: 'large') ).to eq(default_template_output('large_file_url'))
  end

  def default_template_output(url)
    "<figure class='block'><img src='#{url}' /></figure>"
  end

end
