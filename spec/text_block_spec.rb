require 'spec_helper'


describe SirTrevorRenderer::TextBlock do

  before(:each) do
    @block_content = {"text" => "Text Block with **bold**, _italics_ and _**both**_. We also add some #tags and @mentions. This we handle alongside our [links](http://google.com)\n\nwe also have a second paragraph inside this text block\n\n - with\n - list\n\nand more text\n"}
  end

  it 'comes with a default renderer' do
    text_block = SirTrevorRenderer::TextBlock.new()
    expect(text_block.render(@block_content)).to eq( default_output )
  end

  def default_output
    "<p>Text Block with <strong>bold</strong>, <em>italics</em> and <em><strong>both</strong></em>. We also add some #tags and @mentions. This we handle alongside our <a href=\"http://google.com\">links</a></p>\n\n<p>we also have a second paragraph inside this text block</p>\n\n<ul>\n<li>with</li>\n<li>list</li>\n</ul>\n\n<p>and more text</p>\n"
  end

end
