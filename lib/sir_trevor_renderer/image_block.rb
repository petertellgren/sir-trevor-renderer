require 'erb'

module SirTrevorRenderer

  class ImageBlock

    attr_accessor :renderer

    def initialize(options = {})
      @renderer = ERB.new( template )
    end

    def render( block_content, options={} )
      return unless block_content.has_key? 'file'
      file_hash = block_content['file']
      if options[:image_size] && file_hash.has_key?(options[:image_size])
        image_source = file_hash[options[:image_size]]['url']
      else
        image_source = file_hash['url']
      end

      @renderer.result(binding)
    end

    private

      def template
        %{<figure class='block image'><img src='<%= image_source %>' /></figure>}
      end

      def get_image_url(block_content, image_type)
        if json[image_type].present?
          url = json[image_type]['url']
        else
          url = json['url']
        end
      end

  end

end