module SirTrevorRenderer

  class HeadingBlock

    def initialize(optionla_template = nil)
    end

    def render( block_content, options={} )
      <<-HTML
        <div class="block heading">
          <h2>#{block_content['text']}</h2>
        </div>
      HTML
    end

  end

end