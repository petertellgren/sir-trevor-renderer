require 'redcarpet'

module SirTrevorRenderer

  class ListBlock

    attr_accessor :renderer

    def initialize(options = {})
      @renderer = Redcarpet::Markdown.new(Redcarpet::Render::HTML, autolink: true, tables: false)
    end

    def render( block_content, options={} )
      "<div class=\"block list\">#{@renderer.render(block_content['text'])}</div>"
    end
  end

end