module SirTrevorRenderer
  # SirTrevorRenderer::Mapping associates a st-block with a renderer implementation.
  #
  #     mapping = SirTrevorRenderer::Mapping.new
  #     mapping.register(SirTrevorRenderer::HTML::ImageBlock, 'image')
  #     mapping['image'] # => SirTrevorRenderer::HTML::ImageBlock
  #     mapping.new('image', json_data).render
  #
  # You can use {#register} to register a renderer class by type,
  # {#registered?} to see if a block type is mapped,
  # {#[]} to lookup renderers, and {#new} to instantiate render objects
  #
  class Mapping

    attr_reader :blocks

    def initialize
      @blocks = Hash.new
    end

    def initialize_copy(other)
      @blocks = other.blocks.dup
    end

    # Registers a renderer implementation by type name. There can only be
    # one renderer implementation per type, and this method will
    # override any existing mapping.
    #
    # @param block
    # @param block_type [String]
    # @return [void]
    #
    # @example
    #   mapping.register SirTrevorRenderer::ImageBlock, 'image'
    #   mapping['image'] # => SirTrevorRenderer::ImageBlock
    def register(block_renderer, block_type, options={})
      @blocks[block_type.to_s] = block_renderer.new(options)
    end

    # Checks if a block type is registered in this mapping.
    #
    # @param block_type_name [String]
    #
    # @example
    #   mapping.registered?('image')  # => true
    #   mapping.registered?('nope') # => false
    def registered?(block_type)
      @blocks.has_key?(block_type.downcase)
    end

    # Finds a render class based on the block type. Found block returns output
    #
    # @raise [RuntimeError] if there is no render class registered for the
    #   block.
    #
    # @example
    #   mapping.render('image', data, ) # => instance of SirTrevorRenderer::ImageBlock
    #
    # @see SirTrevorRenderer::BaseBlock.new
    def render(block_type, block_content, options={})
      if block_renderer = self[block_type]
        block_renderer.render(block_content, options)
      else
        fail "No renderer registered for #{block_type} block"
      end
    end

    # Looks up a renderer class based on block name
    #
    # @example
    #   mapping['image'] # => SirTrevorRenderer::ImageBlock
    #
    # @return [renderer class]
    def [](block_type)
      lookup(block_type)
    end

    private

      def lookup(block_type)
        @blocks[block_type]
      end

  end
end