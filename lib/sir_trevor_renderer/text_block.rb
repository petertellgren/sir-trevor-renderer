require 'redcarpet'

module SirTrevorRenderer

  class TextBlock

    attr_accessor :renderer

    def initialize( options = {} )
      rdr = Redcarpet::Render::HTML.new(hard_wrap: true)
      @renderer = Redcarpet::Markdown.new(rdr, autolink: true, tables: false)
    end

    def render( block_content, options={} )
      "<div class=\"block text\">#{@renderer.render(block_content['text'])}</div>"
    end
  end

end