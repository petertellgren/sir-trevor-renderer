module SirTrevorRenderer

  class VideoBlock

    attr_reader :services
    def initialize(optionla_template = nil)
      @services = ['vimeo', 'youtube']
    end

    def render( block_content, options={} )
      return false unless @services.include?(block_content['source'])
      send("render_#{block_content['source'].to_s.downcase}", block_content['remote_id'], options[:video_width], options[:video_height])
    end

    private
      def render_vimeo(video_id, width, height)
        "<div class='block video'><iframe src='//player.vimeo.com/video/#{video_id}?title=1&amp;byline=1&amp;portrait=1&amp;autoplay=0' width='#{width}' height='#{height}' frameborder='0' webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe></div>"
      end

      def render_youtube(video_id, width, height)
        "<div class='block video'><iframe width='#{width}' height='#{height}' src='//www.youtube.com/embed/#{video_id}' frameborder='0' allowfullscreen></iframe></div>"
      end
  end

end