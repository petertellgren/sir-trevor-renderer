require 'json'
require 'sir_trevor_renderer/version'
require 'sir_trevor_renderer/mapping'
require 'sir_trevor_renderer/image_block'
require 'sir_trevor_renderer/list_block'
require 'sir_trevor_renderer/heading_block'
require 'sir_trevor_renderer/video_block'
require 'sir_trevor_renderer/text_block'

module SirTrevorRenderer

  @default_mapping = Mapping.new

  def self.render( json, options = {} )
    hash = JSON.parse(json)
    return false unless hash.has_key?("data")
    hash['data'].map { |block|
      block_type = block['type'].to_s.downcase
      default_mapping.render(block_type, block['data'], options)
    }.compact.join
  end

  # @return [SirTrevorRenderer::Mapping] the main mapping object
  def self.default_mapping
    @default_mapping
  end

  # @see SirTrevorRenderer::Mapping#register
  def self.register(renderer_class, block_type)
    default_mapping.register(renderer_class, block_type)
  end

   # @see SirTrevorRenderer::Mapping#registered?
  def self.registered?(block_type)
    default_mapping.registered?(block_type)
  end

  # @see Tilt::Mapping#[]
  def self.[](block_type)
    default_mapping[block_type]
  end

  register(SirTrevorRenderer::HeadingBlock, 'heading')
  register(SirTrevorRenderer::TextBlock, 'text')
  register(SirTrevorRenderer::ListBlock, 'list')
  register(SirTrevorRenderer::ImageBlock, 'image')
  register(SirTrevorRenderer::VideoBlock, 'video')

end