# SirTrevorRenderer

Converts a [Sir Trevor JS](http://madebymany.github.io/sir-trevor-js/) JSON structure to HTML. 

## Installation

Add this line to your application's Gemfile:

    gem 'sir-trevor-renderer'

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install sir-trevor-renderer

## Usage

### Basic usage
```
#!ruby
require 'json'

sir_trevor_json = JSON.generate({data: [{type: "list", data:{ text: " - [item 1](#local)\n - _item 2_\n - **item 3**\n"}}]})

SirTrevorRenderer.render( sir_trevor_json )

```

### Outputs
```
#!html
<ul>
  <li><a href="#local>item 1</a></li>
  <li><em>item 2</em></li>
  <li><strong>item 3</strong></li>
</ul>

```

### Custom blocks
This project comes with default basic renderers for the following Sir Trevor Blocks
- list
- text
- heading
- image
- video

You can easily build your own and add it to the chain by registering your custom renderer

```
#!ruby
class MyCustomBlock
  
  def initialize()

  end

  def render()
    "my custom output"
  end
end

SirTrevorRenderer.register(MyCustomBlock, 'custom-block')
SirTrevorRenderer.render(...)

```